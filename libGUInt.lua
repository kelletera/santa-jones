--/ ---------------------------------------------
--/                  SANTA JONES
--/ ---------------------------------------------
--/ Editor : Alpha Kilo - Games Studio
--/ Developper : Alain Kelleter - alain@ktdev.pro
--/ As Xoran Sorvor - xoran@xoransorvor.be
--/ Website : www.alphakilo.games
--/ Date : 2019
--/ Version : v0.16.1    20122023:20:53
--/ ---------------------------------------------

--/ ----------------------------------------------------
--/ LIB INIT -------------------------------------------
--/ ----------------------------------------------------
local libGUIT = {}

--/ ----------------------------------------------------
--/ LIB METHODES ---------------------------------------
--/ ----------------------------------------------------

local function newElement(pX, pY)
    
    local myElement = {}
    myElement.X = pX
    myElement.Y = pY
    myElement.Visible = true
  
    --myElement.draw = function()
        --print("newElement / draw / Not implemented")
    

    --myElement.update = function(dt)
        --print("newElement / update / Not implemented")
   

    myElement.setVisible = function(pVisible)
        myElement.Visible = pVisible
    end

  return myElement
end --/ END FCT newElement()

function libGUIT.newPanel(pX, pY, pW, pH, pColorOut)
  
    local myPanel = newElement(pX, pY)
    myPanel.W = pW
    myPanel.H = pH
    myPanel.ColorOut = pColorOut
    myPanel.Image = nil
    myPanel.isHover = false
    myPanel.lstEvents = {}
  
    myPanel.setImage = function(pImage)
      myPanel.Image = pImage
      myPanel.W = pImage:getWidth()
      myPanel.H = pImage:getHeight()
    end
    
    myPanel.setEvent = function(pEventType, pFunction)
      myPanel.lstEvents[pEventType] = pFunction
    end
  
    myPanel.updatePanel = function(dt)
      local mx,my = love.mouse.getPosition()
      if mx > myPanel.X and mx < myPanel.X + myPanel.W and my > myPanel.Y and my < myPanel.Y + myPanel.H then
        if myPanel.isHover == false then
          myPanel.isHover = true
          if myPanel.lstEvents["hover"] ~= nil then
              myPanel.lstEvents["hover"]("begin")
          end
        end
      else
        if myPanel.isHover == true then
          myPanel.isHover = false
          if myPanel.lstEvents["hover"] ~= nil then
              myPanel.lstEvents["hover"]("end")
          end
        end
      end
    end --/ END FCT()
  
    myPanel.drawPanel = function()
  
      if myPanel.ColorOut ~= nil then
        love.graphics.setColor(myPanel.ColorOut[1], myPanel.ColorOut[2], myPanel.ColorOut[3])
      else
        love.graphics.setColor(255,255,255)
      end
  
      if myPanel.Image == nil then
        love.graphics.rectangle("line", myPanel.X, myPanel.Y, myPanel.W, myPanel.H)
      else
        love.graphics.setColor(255,255,255)
        love.graphics.draw(myPanel.Image, myPanel.X, myPanel.Y)
      end
  
    end
  
    myPanel.draw = function()
      if myPanel.Visible == false then 
          return 
      end
      myPanel.drawPanel()
    end
    
    myPanel.update = function(dt)
      myPanel.updatePanel()
    end
  
  return myPanel
end --/ END FCT newPanel()

function libGUIT.newText(pX, pY, pW, pH, pText, pFont, pFontSize, pHAlign, pVAlign, pColor)
  
  local myText = libGUIT.newPanel(pX, pY, pW, pH)
  myText.Text = pText
  myText.Font = pFont
  myText.FontSize = pFontSize
  myText.TextW = pFont:getWidth(pText)
  myText.TextH = pFont:getHeight(pText)
  myText.HAlign = pHAlign
  myText.VAlign = pVAlign
  myText.Color = pColor

  myText.drawText = function()
    love.graphics.setFont(myText.Font, myText.FontSize)
    if myText.Color[1] ~= nil then
      love.graphics.setColor(myText.Color[1], myText.Color[2], myText.Color[3])
    else
      love.graphics.setColor(255, 182, 0)
    end

    local x = myText.X
    local y = myText.Y

    if myText.HAlign == "center" then
      x = x + ((myText.W - myText.TextW) / 2)
    end

    if myText.VAlign == "center" then
      y = y + ((myText.H - myText.TextH) / 2)
    end

    love.graphics.print(myText.Text, x, y)
  end

  myText.draw = function()
    if myText.Visible == false then 
        return 
    end
    myText.drawText()
  end
  
  return myText
end --/ END FCT newText()

function libGUIT.newButton(pX, pY, pW, pH, pText, pFont, pColor)
  local myButton = libGUIT.newPanel(pX, pY, pW, pH)
  myButton.Text = pText
  myButton.Font = pFont
  myButton.Label = libGUIT.newText(pX, pY, pW, pH, pText, pFont, "center", "center", pColor)
  myButton.imgDefault = nil
  myButton.imgHover = nil
  myButton.imgPressed = nil
  myButton.isPressed = false
  myButton.oldButtonState = false

  myButton.setImages = function(pImageDefault, pImageHover, pImagePressed)
    myButton.imgDefault = pImageDefault
    myButton.imgHover = pImageHover
    myButton.imgPressed = pImagePressed
    myButton.W = pImageDefault:getWidth()
    myButton.H = pImageDefault:getHeight()
  end

  myButton.update = function(dt)
    myButton.updatePanel(dt)
    if myButton.isHover and love.mouse.isDown(1) and
        myButton.isPressed == false and
        myButton.oldButtonState == false then
        myButton.isPressed = true
      if myButton.lstEvents["pressed"] ~= nil then
        myButton.lstEvents["pressed"]("begin")
      end
    else
      if myButton.isPressed == true and love.mouse.isDown(1) == false then
        myButton.isPressed = false
        if myButton.lstEvents["pressed"] ~= nil then
            myButton.lstEvents["pressed"]("end")
        end
      end
    end
    myButton.oldButtonState = love.mouse.isDown(1)
  end

  myButton.draw = function()
    love.graphics.setColor(255,255,255)
    if myButton.isPressed then
      if myButton.imgPressed == nil then
        myButton.drawPanel()
        love.graphics.setColor(255,255,255,50)
        love.graphics.rectangle("fill", myButton.X, myButton.Y, myButton.W, myButton.H)
      else
        love.graphics.draw(myButton.imgPressed, myButton.X, myButton.Y)
      end
    elseif myButton.isHover then
      if myButton.imgHover == nil then
        myButton.drawPanel()
        love.graphics.setColor(255,255,255)
        love.graphics.rectangle("line", myButton.X+2, myButton.Y+2, myButton.W-4, myButton.H-4)
      else
        love.graphics.draw(myButton.imgHover, myButton.X, myButton.Y)
      end
    else
      if myButton.imgDefault == nil then
        myButton.drawPanel()
      else
        love.graphics.draw(myButton.imgDefault, myButton.X, myButton.Y)
      end    
    end
    myButton.Label.draw()
  end
  
  return myButton
end

function libGUIT.newCheckbox(pX, pY, pW, pH)
  local myCheckbox = libGUIT.newPanel(pX, pY, pW, pH)
  myCheckbox.Text = pText
  myCheckbox.imgDefault = nil
  myCheckbox.imgPressed = nil
  myCheckbox.isPressed = false
  myCheckbox.oldButtonState = false

  myCheckbox.setImages = function(pImageDefault, pImagePressed)
    myCheckbox.imgDefault = pImageDefault
    myCheckbox.imgPressed = pImagePressed
    myCheckbox.W = pImageDefault:getWidth()
    myCheckbox.H = pImageDefault:getHeight()
  end
  
  myCheckbox.setState = function(pbState)
    myCheckbox.isPressed = pbState
  end

  myCheckbox.update = function(dt)
    myCheckbox.updatePanel(dt)
    if myCheckbox.isHover and love.mouse.isDown(1) and
        myCheckbox.isPressed == false and
        myCheckbox.oldButtonState == false then
        myCheckbox.isPressed = true
      if myCheckbox.lstEvents["pressed"] ~= nil then
        myCheckbox.lstEvents["pressed"]("on")
      end
    elseif myCheckbox.isHover and love.mouse.isDown(1) and
        myCheckbox.isPressed == true and
        myCheckbox.oldButtonState == false then
        myCheckbox.isPressed = false
      if myCheckbox.lstEvents["pressed"] ~= nil then
        myCheckbox.lstEvents["pressed"]("off")
      end
    end
    myCheckbox.oldButtonState = love.mouse.isDown(1)
  end

  myCheckbox.draw = function()
    love.graphics.setColor(255,255,255)
    if myCheckbox.isPressed then
      if myCheckbox.imgPressed == nil then
        myCheckbox.drawPanel()
        love.graphics.setColor(255,255,255,50)
        love.graphics.rectangle("fill", myCheckbox.X, myCheckbox.Y, myCheckbox.W, myCheckbox.H)
      else
        love.graphics.draw(myCheckbox.imgPressed, myCheckbox.X, myCheckbox.Y)
      end
    else
      if myCheckbox.imgDefault == nil then
        myCheckbox.drawPanel()
      else
        love.graphics.draw(myCheckbox.imgDefault, myCheckbox.X, myCheckbox.Y)
      end    
    end
  end
  
  return myCheckbox
end

function libGUIT.newProgressBar(pX, pY, pW, pH, pMax, pColorOut, pColorIn)
  local myProgressBar = libGUIT.newPanel(pX, pY, pW, pH)
  myProgressBar.ColorOut = pColorOut
  myProgressBar.ColorIn = pColorIn
  myProgressBar.Max = pMax
  myProgressBar.Value = pMax
  myProgressBar.imgBack = nil
  myProgressBar.imgBar = nil

  myProgressBar.setImages = function(pImageBack, pImageBar)
    myProgressBar.imgBack = pImageBack
    myProgressBar.imgBar = pImageBar
    myProgressBar.W = pImageBack:getWidth()
    myProgressBar.H = pImageBack:getHeight()
  end

  myProgressBar.setValue = function(pValue)
    if pValue >= 0 and pValue <= myProgressBar.Max then
        myProgressBar.Value = pValue
    else
      print("myProgressBar:setValue error - out of range")
    end
  end

  myProgressBar.draw = function()
    love.graphics.setColor(255,255,255)
    local barSize = (myProgressBar.W - 2) * (myProgressBar.Value / myProgressBar.Max)
    if myProgressBar.imgBack ~= nil and myProgressBar.imgBar ~= nil then
      love.graphics.draw(myProgressBar.imgBack, myProgressBar.X, myProgressBar. Y)
      local barQuad = love.graphics.newQuad(0, 0, barSize, myProgressBar.H, myProgressBar.W, myProgressBar.H)
      love.graphics.draw(myProgressBar.imgBar, barQuad, myProgressBar.X, myProgressBar. Y)
    else
        myProgressBar.drawPanel()
      if myProgressBar.ColorOut ~= nil then
        love.graphics.setColor(myProgressBar.ColorIn[1], myProgressBar.ColorIn[2], myProgressBar.ColorIn[3])
      else
        love.graphics.setColor(255,255,255)
      end
      love.graphics.rectangle("fill", myProgressBar.X + 1, myProgressBar.Y + 1, barSize, myProgressBar.H - 2)
    end
  end
  
  return myProgressBar
end

function libGUIT.newGroup()
  local myGroup = {}
  myGroup.elements = {}
  
  myGroup.addElement = function(pElement)
    table.insert(myGroup.elements, pElement)
  end
  
  myGroup.setVisible = function(pVisible)
    for n,v in pairs(myGroup.elements) do
      v.setVisible(pVisible)
    end
  end
  
  myGroup.draw = function()
    love.graphics.push()
    for n,v in pairs(myGroup.elements) do
      v.draw()
    end
    love.graphics.pop()
  end
    
  myGroup.update = function(dt)
    for n,v in pairs(myGroup.elements) do
      v.update(dt)
    end
  end
  
  return myGroup
end

--/ ----------------------------------------------------
--/ LIB return -----------------------------------------
--/ ----------------------------------------------------
return libGUIT
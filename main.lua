--/ ---------------------------------------------
--/                  SANTA JONES
--/ ---------------------------------------------
--/ Editor : Alpha Kilo - Games Studio
--/ Developper : Alain Kelleter - alain@ktdev.pro
--/ As Xoran Sorvor - xoran@xoransorvor.be
--/ Website : www.alphakilo.games
--/ Date : 2019
--/ Version : v0.16.1    20122023:20:53
--/ ---------------------------------------------

--/ ----------------------------------------------------
--/ INIT -----------------------------------------------
--/ ----------------------------------------------------

--/ Autorise ou non l'affichage des traces dans la console pendant l'éxécution
io.stdout:setvbuf('no')

--/ Empèche Love de filtrer les contours des images quand elles sont redimentionnées
--/ Indispensable pour du pixel art
love.graphics.setDefaultFilter("nearest")

--/ Chargement de la librairie de debugging si le mode est actif
local modeDebug = false    -- true or false
if modeDebug then
  --/ Chargement de la librairie libDebug (Debugging Lib)
  oDBG = require("libDebug")
  --/ Initialisation d'une liste de chaînes à débugger
  sDebug = {}
end

--/ Chargement de la librairie libGUInt (Graphic User Interface)
oLGUI = require("libGUInt")

--/ Chargement de la librairie libPForm (Platformer Lib)
oLPF = require("libPForm")

local start

--/ Chargement de la liste des Sprites 
local lLstSprites = oLPF.getLstSprites()

--/ Chargement de la liste des Tuiles
local lLstTiles = oLPF.getLstTiles()
local oWidth = oLPF.getWindow().width
local oHeight = oLPF.getWindow().height

--/ Configuration de la génération de nombres aléatoires
--/ math.randomseed(love.timer.getTime())

--/ Initialisation de l'état isStart (page de démarrage du jeu)
local isStart = oLPF.getWindowStart()
local isStop = oLPF.getWindowStop()

--/ Initialisation du jeu
oLPF.initGame(1)

--/ ----------------------------------------------------
--/ LOVE LOAD ------------------------------------------
--/ ----------------------------------------------------
function love.load()
 
  --/ Initialisation de l'écran de départ (largeur écran, hauteur écran, font size)
  start = oLPF.initStart(oWidth, oHeight, 21)  
  
  --/ Dimensionnement de la fenêtre de jeu
  love.window.setMode(oLPF.getWindow().width, oLPF.getWindow().height)

  --/ Initialisation du titre de la fenêtre du jeu
  love.window.setTitle(oLPF.getWindow().title)

  
end

--/ ----------------------------------------------------
--/ LOVE UPDATE ----------------------------------------
--/ ----------------------------------------------------
function love.update(dt)

  -- Vérifie le changement d'état de window start
  isStart = oLPF.getWindowStart()
  isStop = oLPF.getWindowStop()

  if isStart == false then
    
    if isStop == true then
      stop = oLPF.gameOver(20)
    end
    --/ Parcours la liste des sprites pour mettre à jour les sprites  
    for nSprite=#lLstSprites, 1, -1 do    
      oLPF.updateSprite(lLstSprites[nSprite], dt)
    end

    --/ Parcours la liste des sprites pour vérifier les collisions
    for nSprite = #lLstSprites, 1, -1 do
      local sprite = lLstSprites[nSprite]

      --/ Si le sprite n'est pas un joueur
      if sprite.type ~= "player" then
        --/ Vérifie si il y a collision entre le player et un autre sprite
        if oLPF.checkCollision(
            oLPF.getPlayer().x, 
            oLPF.getPlayer().y, 
            oLPF.getTilesize(), 
            oLPF.getTilesize(), 
            sprite.x, sprite.y, 
            oLPF.getTilesize(), 
            oLPF.getTilesize()) then
            --/ Si collision avec sprite coin  
            if sprite.type == "coin" then
              --/ Suppression du coin la liste de sprites
              table.remove(lLstSprites, nSprite)
              --/ On désincrémente le nombre de coins de 1
              oLPF.setLevelCoins(1)
              --/ Si toutes les pièces sont ramassées
              if oLPF.getLevelCoins() == 0 then
                --/Ouverture de la porte
                oLPF.openDoor()
              end
            --/ Si collision avec sprite porte
            elseif sprite.type == "door" then
              --/ Si toutes les pièces sont ramassées et que l'on entre
              --/ en collision avec la porte(ouverte) on passe au niveau suivant
              if oLPF.getLevelCoins() == 0 then
                oLPF.goNextLevel()
              end 
            elseif sprite.type == "PNJ" then
              local nbLives = oLPF.getnbLives()
              oLPF.delNbLives(nbLives, 1)
              --/ Il faut gérer la perte de vie   
            end
        end
      end

    end  --/ END FOR : Parcours la liste des sprites

  end

  

end

--/ ----------------------------------------------------
--/ LOVE DRAW ------------------------------------------
--/ ----------------------------------------------------
function love.draw()
 
  if isStart == true then    
    oLPF.drawGroup(start)
  else

    if isStop == true then
      oLPF.drawGroup(stop)    
    else

      --/ Initialisation de la police dans les écrans de jeu
      local mainFont = love.graphics.newFont("ui/kenvector_future_thin.ttf", 10)
      love.graphics.setFont(mainFont)

      --/ Mise à l'échelle de l'affichage
      love.graphics.scale(3, 3)

      --/ Dessine les tuiles de la map
      oLPF.drawTiles(oLPF.getMap(), lLstTiles, modeDebug)

      --/ Boucle d'affichage des sprites
      for nSprite=#lLstSprites,1,-1 do    
        oLPF.drawSprite(lLstSprites[nSprite])  
      end

      --/ Affiche le nom du niveau en cours
      local currentLvl = oLPF.getCurrentLevel()
      local nbLives = oLPF.tableLength(oLPF.getnbLives()) 
      local levels = oLPF.getLevels()
      love.graphics.print("Level "..currentLvl..": "..levels[currentLvl], 5, (oLPF.getTilesize() * 18 + 4) - 3)
      love.graphics.print("Lives "..nbLives, 250, (oLPF.getTilesize() * 18 + 4) - 3)
      love.graphics.print("Gifts: "..oLPF.getLevelCoins(), 325, (oLPF.getTilesize() * 18 + 4) - 3)

      --/  Debug section -----------------------------------
      if modeDebug then
        --sDebug.Tuile = " : " .. oLPF.getTileAt(love.mouse.getX(), love.mouse.getY())
        sDebug.Title = " : nothing here"
        --sDebug.Standing =  " : " .. tostring(lLstSprites[1].standing)
        sDebug.isStart =  " : " .. tostring(isStart)     
        
        --/ Display Debug Zone
        oDBG.draw(sDebug, 400, 1, "yellow")
      end --/ End Debug section ------------------------------
    end
  end
    
end

--/ ----------------------------------------------------
--/ LOVE KEYPRESSED ------------------------------------
--/ ----------------------------------------------------
function love.keypressed(key)
  
  if key == "escape" then
    love.event.quit()
  end

  if key == "return" then
    oLPF.setWindowStart(false)     
  end

  if key == "r" then
    oLPF.setWindowStart(true) 
    oLPF.setWindowStop(false)    
  end
  
end
--/ ---------------------------------------------
--/                  SANTA JONES
--/ ---------------------------------------------
--/ Editor : Alpha Kilo - Games Studio
--/ Developper : Alain Kelleter - alain@ktdev.pro
--/ As Xoran Sorvor - xoran@xoransorvor.be
--/ Website : www.alphakilo.games
--/ Date : 2019
--/ Version : v0.16.1    20122023:20:53
--/ ---------------------------------------------

--/ ----------------------------------------------------
--/ LIB INIT -------------------------------------------
--/ ----------------------------------------------------
local LBPf = {}

--/ Déclaration et intialisation des propriétés de la librairie
local map = nil           -- Map du jeu
local lstSprites = {}     -- Liste des Sprites
local level = {}          -- Gestion des éléments du niveau
local levels = {
  " LeapFrog",
  " Building"--,
  --" David M",
  --" Christmas tree"
}                             -- Gestion des niveaux 
local currentLevel = 1        -- Niveau en cours
local nbLives = {1,1,1,1,1}   -- Nombre de vies
local lstTiles = {}           -- Liste des Tuiles

local window = {}         -- Paramètres de la fenêtre
window.start = true
window.stop = false

local player = nil        -- Variable du Player
local bJumpReady = true   -- Variable du saut du Player

--/ Définition des Constantes
local TILESIZE = 15          -- Taille des tuiles (px)
local DEF_GRAVITY = 500      -- Constante de la gravité
local DEF_W_TITLE = "Santa Jones - v0.15.1"     -- Titre fenêtre
local DEF_W_WIDTH = 1125     -- Largeur fenêtre
local DEF_W_HEIGHT = 855     -- Hauteur fenêtre

--/ Initialisation des paramètres de la fenêtre
window.title = DEF_W_TITLE
window.width = DEF_W_WIDTH
window.height = DEF_W_HEIGHT
--/ ----------------------------------------------------
--/ LOADING --------------------------------------------
--/ ----------------------------------------------------

--[[  Initialisation du jeu
      Chargement du niveau passé en paramètre
  ]] 
function LBPf.initGame(pLvl)
 
  
    --/ Chargement des images de tuiles
    LBPf.loadTiles()

    --/ Chargement de la MAP
    map = LBPf.loadMap(pLvl)
    
    --/ Placement des sprites sur la Map
    LBPf.loadSpritesOnMap(map)  

end --/ END FCT initGame()

--/ Chargement des images des tuiles
function LBPf.loadTiles()

  -- Chargement des images des tuiles
  --lstTiles["0"] = love.graphics.newImage("img/tile0.png")
  lstTiles["1"] = love.graphics.newImage("img/tile1.png")
  lstTiles["2"] = love.graphics.newImage("img/tile2.png")
  lstTiles["3"] = love.graphics.newImage("img/tile3.png")
  lstTiles["4"] = love.graphics.newImage("img/tile4.png")
  lstTiles["5"] = love.graphics.newImage("img/tile5.png")
  lstTiles["="] = love.graphics.newImage("img/tile=.png")
  lstTiles["["] = love.graphics.newImage("img/tile[.png")
  lstTiles["]"] = love.graphics.newImage("img/tile].png")
  lstTiles["H"] = love.graphics.newImage("img/tileH.png")
  lstTiles["#"] = love.graphics.newImage("img/tile#.png")
  lstTiles["g"] = love.graphics.newImage("img/tileg.png")
  lstTiles["f"] = love.graphics.newImage("img/tilef.png")
  lstTiles["T"] = love.graphics.newImage("img/tileT.png")
  lstTiles["Y"] = love.graphics.newImage("img/tileY.png")
  lstTiles["U"] = love.graphics.newImage("img/tileU.png")
  lstTiles["C"] = love.graphics.newImage("img/tileC.png")
  lstTiles[">"] = love.graphics.newImage("img/tile-arrow-right.png")
  lstTiles["<"] = love.graphics.newImage("img/tile-arrow-left.png")

end --/ END FCT loadTiles()

--[[  Chargement du niveau passé en paramètre      
      Paramètre (1):  pLvl : identifiant du niveau
  ]]
function LBPf.loadMap(pLvl)
  
    local loadMap = {}   

    --/ Test si level existe
    if pLvl > #levels then
      print("There is no level "..pLvl)
      return
    end
    --/ Si oui le mémorise en tant que level courant
    currentLevel = pLvl
    
    --/ Chargement de la Map 
    local filename = "maps/level"..tostring(pLvl)..".txt"
    for line in love.filesystem.lines(filename) do 
      loadMap[#loadMap + 1] = line
    end
    
    return loadMap
  
end --/ END FCT loadLevel()

--/ Chargement des sprites sur la Map
function LBPf.loadSpritesOnMap(map)
  --/ Init 
  level = {}
  level.playerStart = {}        -- propriétés de départ du player
  level.playerStart.col = 0     -- colonne de départ du player
  level.playerStart.lig = 0     -- ligne de départ du player
  level.coins = 0               -- compteur de pièces/coins

  --/ Parcours la MAP et place les sprites à leur position
  for l = 1, #map do
    for c = 1, #map[1] do

      local char = string.sub(map[l], c, c)

      if char == "P" then
        level.playerStart.col = c
        level.playerStart.lig = l
        player = LBPf.createPlayer(c, l)
      elseif char == "c" then
        LBPf.createCoin(c, l)
        level.coins = level.coins + 1   -- incrémente le compteur de coins
      elseif char == "D" then
        LBPf.createDoor(c, l)
      elseif char == "@" then
        LBPf.createPNJ(c, l)
      end

    end
  end

end --/ END FCT placeOnMap()

--/ Charge et lance le niveau suivant
function LBPf.goNextLevel()
  --/ On incrémente le niveau
  currentLevel = currentLevel + 1
  --/ On test l'existence du niveau sinon on revient au départ
  if currentLevel > #levels then
    currentLevel = 1
    window.stop = true
  end

  --/ Load Map and Sprites 
    --/ Charge la map passée en paramètre
    map = LBPf.loadMap(currentLevel)
    --/ Supprime le player précédent
    LBPf.delPlayer()
    --/ Supprime le(s) PNJ précédents
    LBPf.delPNJ()
    --/ Fermeture de(s) porte(s)
    LBPf.delDoor()
    --/ Charge les sprites de la map (player compris)
    LBPf.loadSpritesOnMap(map)

end --/ END FCT goNextLevel()

--/ Charge la page de lancement du jeu
function LBPf.initStart(pWidth, pHeight, pFontSize)

  local mainFont = love.graphics.newFont("ui/kenvector_future_thin.ttf", pFontSize)
  love.graphics.setFont(mainFont)

  --/ Image Flyer du jeu
  startScreen = oLGUI.newPanel((pWidth/2)-250, (pHeight/2)-350, 500, 500)
  startScreen.setImage(love.graphics.newImage("ui/startscreen.png"))

  --/ Textes de la page de démarrage
  play = oLGUI.newText(startScreen.X + 120, startScreen.Y + 520, 0, 0, "Press Return To Play", mainFont, pFontSize, "", "", {100, 100, 100})
  direction = oLGUI.newText(startScreen.X + 0, startScreen.Y + 555, 0, 0, "Use arrows Up, Left, right to direct you", mainFont, pFontSize, "", "", {157, 164, 174})
  jump = oLGUI.newText(startScreen.X + 0, startScreen.Y + 580, 0, 0, "And Space bar to jump", mainFont, pFontSize, "", "", {157, 164, 174})
  quit = oLGUI.newText(startScreen.X + 0, startScreen.Y + 615, 0, 0, "Esc to quit", mainFont, pFontSize, "", "", {157, 164, 174})
  restart = oLGUI.newText(startScreen.X + 0, startScreen.Y + 640, 0, 0, "R to restart", mainFont, pFontSize, "", "", {157, 164, 174})


  local group = oLGUI.newGroup()

  group.addElement(startScreen)
  group.addElement(play)
  group.addElement(direction)
  group.addElement(jump)
  group.addElement(quit) 
  group.addElement(restart) 

 
  return group  
    
end

--/ Charge la page de fin du jeu
function LBPf.gameOver(pFontSize)

  local mainFont = love.graphics.newFont("ui/kenvector_future_thin.ttf", pFontSize)
  love.graphics.setFont(mainFont)

  gameOver = oLGUI.newText(DEF_W_WIDTH/2 - 80, DEF_W_HEIGHT/2 -100, 0, 0, "GAME OVER", mainFont, pFontSize, "", "", {157, 164, 174})
  restart = oLGUI.newText(DEF_W_WIDTH/2 - 130, DEF_W_HEIGHT/2 -40, 0, 0, "Press R to restart", mainFont, pFontSize, "", "", {157, 164, 174})
  quit = oLGUI.newText(DEF_W_WIDTH/2 - 120, DEF_W_HEIGHT/2 -10, 0, 0, "Press Esc to quit", mainFont, pFontSize, "", "", {157, 164, 174})
  alphakilo = oLGUI.newText(DEF_W_WIDTH/2 - 145, DEF_W_HEIGHT/2 + 70, 0, 0, "www.alphakilo.games", mainFont, pFontSize, "", "", {157, 164, 174})


  local group = oLGUI.newGroup()

  group.addElement(gameOver)
  group.addElement(restart)
  group.addElement(quit)
  group.addElement(alphakilo) 

  return group  
   
end

--/ ----------------------------------------------------
--/ SPRITES --------------------------------------------
--/ ----------------------------------------------------

--[[  Crée un sprite et initialise des propriétés par défaut      
      x : pos X (paramètre pX)
      y : pos Y (paramètre pY)
      vx : vitesse sur l'axe de X
      vy : vitesse sur l'axe de Y
      type = type du sprite (param§tre pType)
      frame : numéro de frame (animation du personnage)
      standing : utile pour savoir si le personnage est sur le décors ou si il tombe
  ]]
function LBPf.createSprite(pType, pX, pY)  
    local mySprite = {}
    
    mySprite.x = pX
    mySprite.y = pY
    mySprite.vx = 0
    mySprite.vy = 0
    mySprite.gravity = 0
    mySprite.collided = false
    mySprite.isJumping = false
    mySprite.type = pType
    mySprite.standing = false
    mySprite.flip = false
        
    mySprite.currentAnimation = ""
    mySprite.frame = 0
    mySprite.animationSpeed = 1/8
    mySprite.animationTimer = mySprite.animationSpeed
    mySprite.animations = {}
    mySprite.images = {}
    
    mySprite.AddImages = function(psDir, plstImage)
      for k,v in pairs(plstImage) do
        local fileName = psDir.."/"..v..".png"
        mySprite.images[v] = love.graphics.newImage(fileName)
      end
    end
    
    mySprite.AddAnimation = function(psDir, psName, plstImages)
      mySprite.AddImages(psDir, plstImages)
      mySprite.animations[psName] = plstImages
    end
    
    mySprite.PlayAnimation = function(psName)
      if mySprite.currentAnimation ~= psName then
        mySprite.currentAnimation = psName
        mySprite.frame = 1
      end
    end

    mySprite.UpdCollided = function (pStatus)
      mySprite.collided = pStatus
    end
    --/ Inse<rtion du sprite et de ses propriétés dans la liste des sprites
    table.insert(lstSprites, mySprite)
  
    --/ retourne le sprite
    return mySprite  
  
end --/ END FCT createSprite()

--/ Crée un sprite de type Player (joueur)
function LBPf.createPlayer(pCol, pLig)

  local myPlayer = LBPf.createSprite("player", (pCol - 1) * TILESIZE, (pLig - 1) * TILESIZE)

  myPlayer.gravity = DEF_GRAVITY
  myPlayer.AddAnimation("img/player", "idle", { "idle1", "idle2", "idle3", "idle4" })
  myPlayer.AddAnimation("img/player", "run", { "run1", "run2", "run3", "run4", "run5", "run6", "run7", "run8", "run9", "run10" })
  myPlayer.AddAnimation("img/player", "climb", { "climb1", "climb2" })
  myPlayer.AddAnimation("img/player", "climb_idle", { "climb1" })
  myPlayer.PlayAnimation("idle")
  bJumpReady = true

  return myPlayer

end --/ END FCT createPlayer()

--/ Crée un sprite de type Coin (pièce) 
function LBPf.createCoin(pCol, pLig)
  local myCoin = LBPf.createSprite("coin", (pCol - 1) * TILESIZE, (pLig - 1) * TILESIZE)
  myCoin.AddAnimation("img/gift", "idle", { "gift1", "gift1", "gift2", "gift2","gift3",  "gift3", "gift4", "gift4", })
  myCoin.PlayAnimation("idle")
end --/ END FCT createCoin()

--/ Crée un sprite de type PNJ
function LBPf.createPNJ(pCol, pLig)
  local myPNJ = LBPf.createSprite("PNJ",(pCol - 1) * TILESIZE, (pLig - 1) * TILESIZE)
  myPNJ.AddAnimation("img/pnj", "walk", { "walk0", "walk1", "walk2", "walk3", "walk4", "walk5" })
  myPNJ.PlayAnimation("walk")
  myPNJ.direction = "right"
  myPNJ.CheckInternalCollision = collidePNJ
end--/ END FCT createPNJ()

--/ Crée un sprite de type door (porte)
function LBPf.createDoor(pCol, pLig)
  local myDoor = LBPf.createSprite("door", (pCol - 1) * TILESIZE, (pLig - 1) * TILESIZE)
  myDoor.AddAnimation("img/door", "close", { "door-close" })
  myDoor.AddAnimation("img/door", "open", { "door-open" })
  myDoor.PlayAnimation("close")
end --/ END FCT createDoor()

--/ Mise à jour des propriétés du sprite pass& en paramètre
function LBPf.updateSprite(pSprite, dt)
  
  --/ Mémorise l'ancienne position
  local oldX = pSprite.x
  local oldY = pSprite.y
  
  --/ Comportement spécifique pour le Player
  if pSprite.type == "player" then
    LBPf.updatePlayer(pSprite, dt) 
  elseif pSprite.type == "PNJ" then
    LBPf.updatePNJ(pSprite, dt)
  end

  --/ Mouvements - On applique la vélocité aux sprites qui se déplécent
  pSprite.x = pSprite.x + pSprite.vx * dt  
  pSprite.y = pSprite.y + pSprite.vy * dt  

  -- Animation
  if pSprite.currentAnimation ~= "" then
    pSprite.animationTimer = pSprite.animationTimer - dt
    if pSprite.animationTimer <= 0 then
      pSprite.frame = pSprite.frame + 1
      pSprite.animationTimer = pSprite.animationSpeed
      if pSprite.frame > #pSprite.animations[pSprite.currentAnimation] then
        pSprite.frame = 1
      end
    end
  end

  --/ Initialise le flag collision
  local collide = false

  --/ Collision à droite
  if pSprite.vx > 0 then
    collide = LBPf.collideRight(pSprite)
  end

  --/ Collsion à gauche
  if pSprite.vx < 0 then
    collide = LBPf.collideLeft(pSprite)
  end

  --/ Si collision alors Stop
  if collide then
    pSprite.vx = 0
    --/ Aligne le sprite sur la colonne 
    LBPf.alignOnColumn(pSprite)
  end

  --/ Réintialisation de collide
  collide = false

  --/ Collision au-dessus
  if pSprite.vy < 0 then
    collide = LBPf.collideTop(pSprite)
    if collide then
      pSprite.vy = 0  
      --/ Aligne le sprite sur la ligne 
      LBPf.alignOnLine(pSprite)
    end
  end
  collide = false

  --/ Collision en-dessous
  if pSprite.standing or pSprite.vy > 0 then

    collide = LBPf.collideDown(pSprite)
    if collide then
      pSprite.standing = true
      pSprite.vy = 0 
      --/ Aligne le sprite sur la ligne 
      LBPf.alignOnLine(pSprite)
    else
      --/ On vérifie que l'on a pas à faire à un sprite volant
      --/ Si le sprite vole alors gravity == 0 et donc n'est pas posé (standing)
      if pSprite.gravity ~= 0 then
        pSprite.standing = false
      end
      pSprite.standing = false
    end
  end

  -- Sprite falling
  if pSprite.standing == false then
    pSprite.vy = pSprite.vy + pSprite.gravity * dt
  end

  --/ Mouvements - On applique la vélocité aux sprites qui se déplacent
  --pSprite.x = pSprite.x + pSprite.vx * dt
  --pSprite.y = pSprite.y + pSprite.vy * dt

end --/ END FCT updateSprite()

--[[  Mise à jour du player      
      * Applique une friction sur la vélocité du personnage (effet de glissade qui ralenti)
      * Test le clavier pour augmenter la vélocité si le joueur se déplace ou saute
      pour le saut, on applique simplement une vélocité négative d'un seul coup
      * Applique la vélocité à la position du personnage
  ]]
function LBPf.updatePlayer(pPlayer, dt)
    --/ Déclarations et intialisations
    local accel = 350
    local friction = 120 
    local maxSpeed = 100
    local jumpVelocity = -190
  
    --/ Détermine la tuile sous le player
      --/ Tuile qui se trouve sous les pieds du personnage
      local idUnder = LBPf.getTileAt(pPlayer.x + TILESIZE/2, pPlayer.y + TILESIZE)
      --/ Tuile qui se trouve sous le personnage (son centre)
      local idOverlap = LBPf.getTileAt(pPlayer.x + TILESIZE/2, pPlayer.y + TILESIZE-1)
  
    -- Stop Jump - gérer l'arrêt du saut
    if pPlayer.isJumping and (LBPf.collideDown(pPlayer) or LBPf.isLadder(idUnder)) then
      pPlayer.isJumping = false
      pPlayer.standing = true
      LBPf.alignOnLine(pPlayer)
    end
  
    --/ Update Friction -----------------------------------
      --/ Si le joueur va vers la droite
      if pPlayer.vx > 0 then
        --/ On limite sa vitesse en applicant une friction
        pPlayer.vx = pPlayer.vx - friction * dt
        --/ On l'arrête
        if pPlayer.vx < 0 then pPlayer.vx = 0 end
      end
      
      --/ Si le joueur va vers la gauche
      if pPlayer.vx < 0 then
        --/ On limite sa vitesse en applicant une friction
        pPlayer.vx = pPlayer.vx + friction * dt
        --/ On l'arrête
        if pPlayer.vx > 0 then pPlayer.vx = 0 end
      end
    --/ ---------------------------------------------------
  
    --/  Par défaut le player n'est pas en mouvement
    local newAnimation = "idle"
  
    --/ Gestion du clavier 
      --/ Vélocité droite
      if love.keyboard.isDown("right") then
        pPlayer.vx = pPlayer.vx + accel * dt
        --/ Limitation de la vitesse à doite
        if pPlayer.vx > maxSpeed then pPlayer.vx = maxSpeed end
        pPlayer.flip = false
        newAnimation = "run"
      end
      --/ Vélocité gauche
      if love.keyboard.isDown("left") then
        pPlayer.vx = pPlayer.vx - accel * dt
        --/ Limitation de la vitesse à gauche
        if pPlayer.vx < -maxSpeed then pPlayer.vx = -maxSpeed end
        pPlayer.flip = true
        newAnimation = "run"
      end
  
      --/ Vérifier si le joueur chevauche une échelle
      local isOnLadder = LBPf.isLadder(idUnder) or LBPf.isLadder(idOverlap)
      if LBPf.isLadder(idOverlap) == false and LBPf.isLadder(idUnder) then
        pPlayer.standing = true
      end
  
      --/ Si jump 
      if love.keyboard.isDown("space") and pPlayer.standing and bJumpReady then
        pPlayer.isJumping = true
        pPlayer.gravity = DEF_GRAVITY
        pPlayer.vy = jumpVelocity
        pPlayer.standing = false
        bJumpReady = false
      end
  
      -- Grimper (Climb)
      if isOnLadder and pPlayer.isJumping == false then
        pPlayer.gravity = 0
        pPlayer.vy = 0
        bJumpReady = false
      end
  
      --/ On prépare une position "en train de grimper" si on est au milieu d'une échelle
      if LBPf.isLadder(idUnder) and LBPf.isLadder(idOverlap) then
        newAnimation = "climb_idle"
      end
  
      --/ Si le joueur presse sur la touche "up" et qu'on est sur une échelle (à ses pieds, ou en plein milieu), 
      --/ on fixe une vélocité verticale négative et on prépare l'animation correspondante 
      if love.keyboard.isDown("up") and isOnLadder == true and pPlayer.isJumping == false then
        pPlayer.vy = -50
        newAnimation = "climb"
      end
  
      --/ Si le joueur veut descendre 
      if love.keyboard.isDown("down") and isOnLadder == true then
        pPlayer.vy = 50
        newAnimation = "climb"
      end
      
      --/ Si on n'est pas sur une échelle (mais qu'on y a été), il faut vite remettre la gravité
      -- Not climbing
      if isOnLadder == false and pPlayer.gravity == 0 and 
        pPlayer.isJumping == false then
        pPlayer.gravity = DEF_GRAVITY
      end
  
      -- Prêt pour le prochain saut
      if love.keyboard.isDown("up") == false and pPlayer.standing and bJumpReady == false then
        bJumpReady = true
      end
  
    --/ ---------------------------------------------------
    pPlayer.PlayAnimation(newAnimation)

end --/ END FCT updatePlayer

--/ Mise à jour d'un PNJ
function LBPf.updatePNJ(pSprite, dt)
  
  --/ Tuile en-dessous du sprite (selon son centre)
  local idOverlap = LBPf.getTileAt(pSprite.x + TILESIZE/2, pSprite.y + TILESIZE-1)
  
  --/ Retourne le sprite
  if idOverlap == ">" then
    pSprite.direction = "right"
    pSprite.flip = false
  elseif idOverlap == "<" then
    pSprite.direction = "left"
    pSprite.flip = true
  end
  --/ Change la sens de marche du PNJ
  if pSprite.direction == "right" then
    pSprite.vx = 25
  elseif pSprite.direction == "left" then
    pSprite.vx = -25
  end

end --/ END FCT updatePNJ()

--/ Alignement du sprite sur sa ligne
function LBPf.alignOnLine(pSprite)
  local lig = math.floor((pSprite.y + TILESIZE/2) / TILESIZE) + 1
  pSprite.y = (lig - 1) * TILESIZE
end --/ END FCT AlignOnLine()

--/ Alignement du sprite sur sa colonne
function LBPf.alignOnColumn(pSprite)
  local col = math.floor((pSprite.x + TILESIZE/2) / TILESIZE) + 1
  pSprite.x = (col - 1) *  TILESIZE
end --/ END FCT AlignOnColumn()

--/ ----------------------------------------------------
--/ TESTS ----------------------------------------------
--/ ----------------------------------------------------

--/ Retourne si la tuile est solide ou non
function LBPf.isSolid(pID)
  if pID == "0" then return false end
  if pID == "1" then return true end
  if pID == "5" then return true end
  if pID == "4" then return true end
  if pID == "=" then return true end
  if pID == "[" then return true end
  if pID == "]" then return true end
  if pID == "C" then return true end
  if pID == "T" then return true end
  if pID == "Y" then return true end
  if pID == "U" then return true end

  
  return false
end --/ END FCT isSolid()

--/ Retourne true si c'est une tuile "Jump Through" (sauter au travers)
function LBPf.isJumpThrough(pID)
  if pID == "g" then return true end
  return false
end --/ END isJumpThrough()

--/ Retourne true si la tuile passée en paramètre est une échelle
function LBPf.isLadder(pID)
  if pID == "H" then 
    return true 
  elseif pID == "#" then 
      return true
  end
  return false
end --/ END FCT isLadder()

--/ Retourne true si c'est un charactère dit invisible
function LBPf.isInvisible(pID)
  if pID == ">" or pID == "<" then 
    return true 
  end
  return false
end

--/ ----------------------------------------------------
--/ TOOLS ----------------------------------------------
--/ ----------------------------------------------------

--/ Retourne  le nombré d"éléments d'un tableau ou d'une liste
function LBPf.tableLength(pList)
  local count = 0
  for _ in pairs(pList) do count = count + 1 end
  return count
end

--/ ----------------------------------------------------
--/ COLLISIONS -----------------------------------------
--/ ----------------------------------------------------

--[[  Notes
      Les fonctions de collisions testent 2 points (hotspots), à droite, gauche,
      en haut ou en bas. Elles utilisent la fonction isSolid() qui retourne, 
      pour un id de tuile donné, si celle-ci est solide ou pas

      Le +30 pour les collisions verticales est important. 
      Il positionne le hotspot à l'intérieur sur sprite, juste à un pixel du bord, 
      ce qui évite les fausses collisions en cas de saut en diagonale à ras d'une tuile.
  ]]

--/ Détection collsion droite
function LBPf.collideRight(pSprite)
  local id1 = LBPf.getTileAt(pSprite.x + TILESIZE, pSprite.y + 3)
  local id2 = LBPf.getTileAt(pSprite.x + TILESIZE, pSprite.y + TILESIZE -2)
  if LBPf.isSolid(id1) or LBPf.isSolid(id2) then return true end
  return false  
end --/ END FCT collideRight()

--/ Détection collsion gauche
function LBPf.collideLeft(pSprite)
  local id1 = LBPf.getTileAt(pSprite.x-1, pSprite.y + 3)
  local id2 = LBPf.getTileAt(pSprite.x-1, pSprite.y + TILESIZE - 2)
  if LBPf.isSolid(id1) or LBPf.isSolid(id2) then return true end
  return false
end --/ END FCT collideLeft()

--/ Détection collsion en bas
function LBPf.collideDown(pSprite)

  local id1 = LBPf.getTileAt(pSprite.x + 2, pSprite.y + TILESIZE)
  local id2 = LBPf.getTileAt(pSprite.x + TILESIZE - 3, pSprite.y + TILESIZE)

  if LBPf.isSolid(id1) or LBPf.isSolid(id2) then return true end

  if LBPf.isJumpThrough(id1) or LBPf.isJumpThrough(id2) then
    local lig = math.floor((pSprite.y + TILESIZE/2) / TILESIZE) + 1
    local yLine = (lig - 1) * TILESIZE
    local distance = pSprite.y - yLine
    if distance >= 0 and distance < 10 then
      return true
    end
  end

  return false
end --/ END FCT collideDown()

--/ Détection collsion en haut
function LBPf.collideTop(pSprite)
  local id1 = LBPf.getTileAt(pSprite.x + 1, pSprite.y - 1)
  local id2 = LBPf.getTileAt(pSprite.x + TILESIZE - 2, pSprite.y - 1)
  if LBPf.isSolid(id1) or LBPf.isSolid(id2) then return true end
  return false
end --/ END FCT collideTop()

--/ Détection collision avec un PNJ
function LBPf.collidePNJ(pSprite, dt)
  -- Tile under the player
  local idUnder = getTileAt(pSprite.x + TILESIZE/2, pSprite.y + TILESIZE)
  local idOverlap = getTileAt(pSprite.x + TILESIZE/2, pSprite.y + TILESIZE-1)
  
  pSprite.vx = 0
  local isCollide = false
  
  if idOverlap == ">" then
    pSprite.direction = "right"
    pSprite.flip = false
    pSprite.x = pSprite.x + 2
    isCollide = true
  elseif idOverlap == "<" then
    pSprite.direction = "left"
    pSprite.flip = true
    pSprite.x = pSprite.x - 2
    isCollide = true
  end
    
  return isCollide
end --/ END FCT collidePNJ()

--/ Détection des collisions entre le player et les autres sprites
--/ Src: https://love2d.org/wiki/BoundingBox.lua
function LBPf.checkCollision(x1, y1, w1, h1, x2, y2, w2, h2)
  return x1 < x2 + w2 and
         x2 < x1 + w1 and
         y1 < y2 + h2 and
         y2 < y1 + h1
end

--/ ----------------------------------------------------
--/ ACTIONS --------------------------------------------
--/ ----------------------------------------------------

--/ Ouverture de(s) porte(s) fermée(s) 
function LBPf.openDoor()
  for nSprite=#lstSprites, 1, -1 do
    local sprite = lstSprites[nSprite]
    if sprite.type == "door" then
      sprite.PlayAnimation("open")
    end
  end
end--/ END FCT openDoor()

--/ Fermeture de(s) porte(s) ouverte(s) 
function LBPf.closeDoor()
  for nSprite=#lstSprites, 1, -1 do
    local sprite = lstSprites[nSprite]
    if sprite.type == "door" then
      sprite.PlayAnimation("close")
    end
  end
end--/ END FCT closeDoor()

--/ Supprime le player 
function LBPf.delPlayer()
  for nSprite=#lstSprites, 1, -1 do
    local sprite = lstSprites[nSprite]
    if sprite.type == "player" then
      table.remove(lstSprites, nSprite )
    end
  end
end --/ END FCT delPlayer()

--/ Supprime la porte 
function LBPf.delDoor()
  for nSprite=#lstSprites, 1, -1 do
    local sprite = lstSprites[nSprite]
    if sprite.type == "door" then
      table.remove(lstSprites, nSprite )
    end
  end
end --/ END FCT delPlayer()

--/ Supprime le player 
function LBPf.delPNJ()
  for nSprite=#lstSprites, 1, -1 do
    local sprite = lstSprites[nSprite]
    if sprite.type == "PNJ" then
      table.remove(lstSprites, nSprite)
    end
  end
end --/ END FCT delPNJ()

--/ ----------------------------------------------------
--/ DRAWING --------------------------------------------
--/ ----------------------------------------------------

--[[  Dessine les tuiles à l'écran à l'aide d'une double boucle      
      Paramètre (1):  map : la map du niveau
      Paramètre (2):  tileMaps : les tuiles
  ]]
function LBPf.drawTiles(map, lstTiles, modeDebug)

    for l = 1,#map do
        for c = 1,#map[1] do

          --/ Découpe la chaine de caractères (string.sub(1er_caractère, dernier_caractère))
          local char = string.sub(map[l], c, c)

          --/ Gestion des sprites invisible en fonction du modedebug
          local isInvisible
          --/ On test l'invisiblité du sprite
          isInvisible = LBPf.isInvisible(char)
          --/ On force l'affichage en modedebug == true
          if modeDebug then isInvisible = false end
          
          --/ Si la valeur numérique n'est pas égale à 0 et si n'est pas invisible (false) alors on affiche l'élément
          if tonumber(char) ~= "0" and isInvisible == false then
            if lstTiles[char] ~= nil then
              love.graphics.draw(lstTiles[char],(c - 1) * TILESIZE, (l - 1) * TILESIZE)
            end
          end

        end
    end

end --/ END FCT drawTiles()

--/ Dessine les sprites à l'écran
function LBPf.drawSprite(pSprite)
  local imgName = pSprite.animations[pSprite.currentAnimation][pSprite.frame]
  local img = pSprite.images[imgName]
  local halfw = img:getWidth() / 2
  local halfh = img:getHeight() / 2
  local flipCoef = 1
  if pSprite.flip then flipCoef = -1 end
    love.graphics.draw(
    img, -- Image
    pSprite.x + halfw, -- horizontal position
    pSprite.y + halfh, -- vertical position
    0, -- rotation (none = 0)
    1 * flipCoef, -- horizontal scale
    1, -- vertical scale (normal size = 1)
    halfw, halfh -- horizontal and vertical offset
    )  
end

--/ Dessine le groupe passé en paramètre
function LBPf.drawGroup(group)
  group.draw();
end

--/ ----------------------------------------------------
--/ GETTERS --------------------------------------------
--/ ----------------------------------------------------

--[[  Retourne l'identifiant de la tuile qui se situe aux coordonnées passées en paramètres      
      Paramètre (1):  pX : position de la souris sur l'axe des X
      Paramètre (2):  pY : position de la souris sur l'axe des Y
      LBPf.MAP =  Map chargée dabs la fonction initGame
  ]]
function LBPf.getTileAt(pX, pY)
    local col = math.floor(pX / TILESIZE) + 1
    local lig = math.floor(pY / TILESIZE) + 1
    --/ Si on est pas en dehors de la Map
    if col>0 and col<=#map[1] and lig>0 and lig<=#map then
      local id = string.sub(map[lig],col,col)
      return id
    end
  
    return 0
end --/ END FCT getTitleAt()

--/ Getter Map
function LBPf.getMap()
  return map
end --/ END FCT getLstSprites()

--/ Getter Player
function LBPf.getPlayer()
  
  local player = {}
  for nSprite = #lstSprites, 1, -1 do
    local sprite = lstSprites[nSprite]
    if sprite.type == "player" then
      player.x = sprite.x 
      player.y = sprite.y 
      --/ Ajout d'autres propriétés si besoin...
    end
  end
  return player

end --/ END FCT getPlayer()

--/ Getter lstSprites
function LBPf.getLstSprites()
  return lstSprites
end --/ END FCT getLstSprites()

--/ Getter lstTiles
function LBPf.getLstTiles()
  return lstTiles
end --/ END FCT getlstTiles()

--/ Getter level.coins
function LBPf.getLevelCoins()
  return level.coins
end --/ END FCT getLevelCoins()

--/ Getter currentLevel
function LBPf.getCurrentLevel()
  return currentLevel
end --/ END FCT getCurrentLevel()

--/ Getter levels
function LBPf.getLevels()
  return levels
end --/ END FCT getLevels()

--/ Getter NbLives
function LBPf.getnbLives()
  return nbLives
end --/ END FCT getnbLives()

--/ Getter window
function LBPf.getWindow()
  return window
end --/ END FCT getWindow()

--/ Getter window.start
function LBPf.getWindowStart()
  return window.start 
end --/ END FCT getWindowStart()

--/ Getter window.stop
function LBPf.getWindowStop()
  return window.stop 
end --/ END FCT getWindowStop()

--/ Getter TILESIZE
function LBPf.getTilesize()
  return TILESIZE
end --/ END FCT getTilesize()

--/ ----------------------------------------------------
--/ SETTERS --------------------------------------------
--/ ----------------------------------------------------

--/ Mise à jours du nombre de Coins
function LBPf.setLevelCoins(nb)
  level.coins = level.coins - nb
end

--/ Mise à jours du nombre de vies
function LBPf.delNbLives(nbLives)
  table.remove(nbLives, 1)
end

--/ Mise à jours de window.start
function LBPf.setWindowStart(pStart)
  window.start = pStart
end

--/ Mise à jours de window.stop
function LBPf.setWindowStop(pStart)
  window.stop = pStart
end
--/ ----------------------------------------------------
--/ LIB RETURN -----------------------------------------
--/ ----------------------------------------------------
return LBPf
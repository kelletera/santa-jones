# Santa Jones

### [English description]

Santa Jones is a genetic cross between our good old Santa Claus and another well-known old adventurer, a certain "Indiana Jones".

We will have to guide our Santa Jones through different platforms and avoid pitfalls to enable him to carry out his mission successfully!

### [French description]

Santa jones, c'est un croisement génétique entre notre bon vieux Père Noël et un autre vieux bourlingeur  bien connu, un certain  « Indiana Jones ».

Il faudra guider notre Santa Jones à travers différentes plateformes et éviter des embûches pour lui permettre de mener à bien sa mission !

### [General informations]

**Warning, actually this source code is not functionnal, for developper only  (this project is under development).**

* Version : 0.16.1 beta
* Environment : Lua / Löve
* License : GNU GPL v3 (see LICENSE file)

### [Flyer]

![Santa Jones Flyer](img/santa-jones-flyer.jpg "Santa Jones")




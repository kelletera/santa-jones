# Remerciements

Je remercie David Mekersa et sa formation sans lesquels ce prototype de platformer n'aurait pu voir le jour.

Le code source de ce jeu est en grande partie inspiré par la formation sur la conception d'un jeu de type
plates-forme en pure code (Lua/LÖVE): https://www.gamecodeur.fr/liste-ateliers/atelier-platformer-love2d

N'hésitez pas à allez y jeter un oeil et à vous y inscrire si vous êtes passionné par l'écriture de jeux vidéo en pure code!

Rendez-vous sur: https://www.gamecodeur.fr

# Acknowledgements

I would like to thank David Mekersa and his team without whom this prototype platform would not have been possible.

The source code of this game is largely inspired by the training on how to design a game of the type
platforms in pure code (Lua/LÖVE): https://www.gamecodeur.fr/liste-ateliers/atelier-platformer-love2d

Feel free to check it out and register if you are passionate about writing pure code video games!

Visit: https://www.gamecodeur.fr